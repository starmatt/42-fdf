/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/02 17:23:44 by mborde            #+#    #+#             */
/*   Updated: 2015/02/08 17:06:06 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# include "libft.h"
# include "get_next_line.h"
# include <mlx.h>
# include <unistd.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <fcntl.h>
# include <stdlib.h>
# define WIN_X 1920
# define WIN_Y 1080

typedef	struct	s_pxl
{
	float	x;
	float	y;
	int		z;
}				t_pxl;

typedef	struct	s_env
{
	char	*argv;
	void	*mlx;
	void	*win;
	void	*img;
	int		fd;
	t_pxl	***map;
	int		nb_line;
	int		line_max;
	char	*map_nb;
	char	*data_addr;
	int		bpp;
	int		sizeline;
	int		endian;
	int		len;
	int		x;
	int		y;
	int		z;
	int		color;
	float	rotate;
	int		toggle;
}				t_env;

void			stock_map(t_env *e);
int				nb_line(t_env *e);
int				line_len(t_env *e);
void			create_img(t_env *e);
void			draw_on_img(t_env *e);
void			update_map(t_env *e);
void			print_exit(char *s, int value, int fd);
int				key_hook(int k, t_env *e);
int				get_color(t_pxl *p1, t_pxl *p2, t_env *e);
void			show_menu(t_env *e);

#endif
