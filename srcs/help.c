/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/04 17:35:06 by mborde            #+#    #+#             */
/*   Updated: 2015/02/09 18:09:54 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	reset(t_env *e)
{
	e->x = 0;
	e->y = 0;
	e->z = 2;
	e->rotate = 2;
	e->len = 20;
}

static void	change_color(int k, t_env *e)
{
	int	tmp_color;

	tmp_color = e->color;
	if (k == 49)
		e->color = 0;
	else if (k == 50)
		e->color = 1;
	else if (k == 51)
		e->color = 2;
	else if (k == 52)
		e->color = 3;
	if (e->color != tmp_color)
		create_img(e);
}

static void	key_hook_again(int k, t_env *e)
{
	if (k == 65307)
		print_exit("\tExit: closed by user.", 0, 1);
	else if (k == 113 && e->rotate >= 0.1)
		e->rotate -= 0.1;
	else if (k == 101)
		e->rotate += 0.1;
	else if (k == 32)
		reset(e);
	else if (k == 65289)
		e->toggle = (e->toggle) ? 0 : 1;
	if (k == 32 || k == 113 || k == 101 || k == 65289)
	{
		update_map(e);
		create_img(e);
	}
}

int			key_hook(int k, t_env *e)
{
	if (k == 105)
		e->z--;
	else if (k == 111)
		e->z++;
	else if (k == 115)
		e->y++;
	else if (k == 97)
		e->x--;
	else if (k == 100)
		e->x++;
	else if (k == 119)
		e->y--;
	else if (k == 61 && e->len <= 40)
		e->len += e->len;
	else if (k == 45)
		e->len -= e->len / 2;
	if (k == 105 || k == 111 || k == 100 || k == 119 || k == 97 || k == 115 ||
			k == 61 || k == 45)
	{
		update_map(e);
		create_img(e);
	}
	change_color(k, e);
	key_hook_again(k, e);
	return (0);
}
