/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stock_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/02 17:57:26 by mborde            #+#    #+#             */
/*   Updated: 2015/02/09 21:37:32 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void			update_map(t_env *e)
{
	int	x;
	int	y;
	int	x_tmp;
	int	y_tmp;

	y = 0;
	while (e->map[y])
	{
		x = 0;
		while (e->map[y][x])
		{
			y_tmp = y - e->nb_line / 2;
			x_tmp = x - e->line_max / 2;
			x_tmp *= 2;
			e->map[y][x]->x = (x_tmp * e->len) + (y_tmp * e->len * 2)
				+ (e->x * 30);
			e->map[y][x]->y = (y_tmp * e->len) - (x_tmp * e->len / e->rotate)
				- e->map[y][x]->z * e->z * e->len / 5 + (e->y * 30);
			e->map[y][x]->x += WIN_X / 2;
			e->map[y][x]->y += WIN_Y / 2;
			x++;
		}
		y++;
	}
}

static t_pxl	*parse_map(int y, int x, int map_nb, t_env *e)
{
	t_pxl	*pixel;

	pixel = NULL;
	if ((pixel = (t_pxl *)malloc(sizeof(t_pxl))))
	{
		y -= e->nb_line / 2;
		x -= e->line_max / 2;
		x *= 2;
		pixel->x = (x * e->len) + (y * e->len * 2);
		pixel->y = (y * e->len) - (x * e->len / 2) - map_nb * e->z * e->len / 5;
		pixel->z = map_nb;
		pixel->x += WIN_X / 2;
		pixel->y += WIN_Y / 2;
	}
	else
		print_exit("\tError: (parse_map) malloc has failed.", -1, 2);
	return (pixel);
}

int				line_len(t_env *e)
{
	unsigned int	i;
	char			**split;
	char			*line;
	int				line_len;

	i = 0;
	while (get_next_line(e->fd, &line))
	{
		if (ft_strlen(line) > i)
		{
			i = ft_strlen(line);
			split = ft_strsplit(line, ' ');
		}
	}
	line_len = 0;
	while (split[line_len])
		line_len++;
	free(*split);
	e->fd = open(e->argv, O_RDWR);
	return (line_len);
}

int				nb_line(t_env *e)
{
	int		nb;
	char	*line;

	nb = 0;
	while (get_next_line(e->fd, &line))
		nb++;
	e->fd = open(e->argv, O_RDWR);
	return (nb);
}

void			stock_map(t_env *e)
{
	char	*l;
	char	**split;
	int		i[3];

	i[1] = 0;
	if (!(e->map = (t_pxl ***)malloc(sizeof(t_pxl **) * e->nb_line)))
		print_exit("\tError: (stock_map) malloc has failed.", -1, 2);
	while (get_next_line(e->fd, &l) && (split = ft_strsplit(l, ' ')))
	{
		if (!(e->map[i[1]] = (t_pxl **)malloc(sizeof(t_pxl *) * ft_strlen(l))))
			print_exit("\tError: (stock_map) malloc has failed.", -1, 2);
		i[0] = 0;
		i[2] = 0;
		while ((e->map_nb = split[i[2]]))
		{
			e->map[i[1]][i[0]] = parse_map(i[1], i[0], ft_atoi(e->map_nb), e);
			i[0]++;
			i[2]++;
		}
		e->map[i[1]][i[0]] = NULL;
		free(*split);
		i[1]++;
		e->map[i[1]] = NULL;
	}
}
