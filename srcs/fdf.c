/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/05 17:33:25 by mborde            #+#    #+#             */
/*   Updated: 2015/02/08 17:03:49 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	set_pxl(int x, int y, unsigned int color, t_env *e)
{
	int	r;
	int	g;
	int	b;

	r = (color & 0xFF0000) >> 16;
	g = (color & 0xFF00) >> 8;
	b = color & 0xFF;
	x *= 2;
	if (x >= 0 && y >= 0 && x < WIN_X * 2 && y <= WIN_Y)
	{
		e->data_addr[(y * e->sizeline) + (2 * x) + 2] = r;
		e->data_addr[(y * e->sizeline) + (2 * x) + 1] = g;
		e->data_addr[(y * e->sizeline) + (2 * x)] = b;
	}
}

static void	set_line(t_pxl *p1, t_pxl *p2, t_env *e)
{
	float	x;
	float	y;
	int		color;

	color = get_color(p1, p2, e);
	x = p1->x;
	while (x < p2->x)
	{
		y = p1->y + (((p2->y - p1->y) * (x - p1->x)) / (p2->x - p1->x));
		set_pxl(x, y, color, e);
		x += 0.1;
	}
}

void		draw_on_img(t_env *e)
{
	int	x;
	int	y;

	y = 0;
	while (e->map[y])
	{
		x = 0;
		while (e->map[y][x])
		{
			if (x > 0 && e->map[y][x - 1])
				set_line(e->map[y][x - 1], e->map[y][x], e);
			if (y > 0 && e->map[y - 1][0] && e->map[y - 1][x])
				set_line(e->map[y - 1][x], e->map[y][x], e);
			x++;
		}
		y++;
	}
}

void		create_img(t_env *e)
{
	if (!(e->img = mlx_new_image(e->mlx, WIN_X, WIN_Y)))
		print_exit("\tError: (create_img) image could not be created.", -1, 2);
	else
	{
		if (!(e->data_addr = mlx_get_data_addr
					(e->img, &e->bpp, &e->sizeline, &e->endian)))
			print_exit("\tError: (create_img) mlx_get_data_addr has failed.",
					-1, 2);
		else
			draw_on_img(e);
	}
	mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
	mlx_destroy_image(e->mlx, e->img);
	show_menu(e);
}
