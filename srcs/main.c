/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/02 17:57:12 by mborde            #+#    #+#             */
/*   Updated: 2015/02/10 04:28:15 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		print_exit(char *s, int value, int fd)
{
	ft_putendl_fd(s, fd);
	exit (value);
}

static int	expose_hook(t_env *e)
{
	create_img(e);
	return (0);
}

void		show_menu(t_env *e)
{
	if (e->toggle)
	{
		mlx_string_put(e->mlx, e->win, 20, 40, 0x666666, "FDF:");
		mlx_string_put(e->mlx, e->win, 20, 55, 0xFFFFFF, e->argv);
		mlx_string_put(e->mlx, e->win, 20, 80, 0x666666, "HELP:");
		mlx_string_put(e->mlx, e->win, 20, 95, 0xFFFFFF, "toggle......TAB");
		mlx_string_put(e->mlx, e->win, 20, 110, 0xFFFFFF, "move........WASD");
		mlx_string_put(e->mlx, e->win, 20, 125, 0xFFFFFF, "depth.......I/O");
		mlx_string_put(e->mlx, e->win, 20, 140, 0xFFFFFF, "zoom........-/+");
		mlx_string_put(e->mlx, e->win, 20, 155, 0xFFFFFF, "reset.......SPACE");
		mlx_string_put(e->mlx, e->win, 20, 170, 0xFFFFFF, "exit........ESC");
		mlx_string_put(e->mlx, e->win, 20, 195, 0x666666, "THEME:");
		mlx_string_put(e->mlx, e->win, 20, 210, 0xFFFFFF, "earth.......1");
		mlx_string_put(e->mlx, e->win, 20, 225, 0xFFFFFF, "forest......2");
		mlx_string_put(e->mlx, e->win, 20, 240, 0xFFFFFF, "shadow......3");
		mlx_string_put(e->mlx, e->win, 20, 255, 0xFFFFFF, "beach.......4");
	}
}

static void	init_all(t_env *e)
{
	e->toggle = 1;
	e->map = NULL;
	e->img = NULL;
	e->data_addr = NULL;
	e->map_nb = NULL;
	e->z = 2;
	e->len = 20;
	e->sizeline = 0;
	e->bpp = 0;
	e->rotate = 2;
	if ((e->nb_line = nb_line(e)) == 0)
		print_exit("\tError: invalid file.", -1, 2);
	if ((e->line_max = line_len(e)) == 0)
		print_exit("\tError: invalid file.", -1, 2);
	if (!(e->win = mlx_new_window(e->mlx, WIN_X, WIN_Y,
					"Un fil, du fer. Un fil de fer.")))
		print_exit("\tError: window has failed opening.", -1, 2);
	e->color = 0;
	stock_map(e);
	mlx_expose_hook(e->win, expose_hook, e);
	mlx_hook(e->win, 3, 3, key_hook, e);
	mlx_loop(e->mlx);
}

int			main(int argc, char **argv)
{
	t_env	e;

	if (argc >= 2)
	{
		if ((e.fd = open(argv[1], O_RDWR)) == -1)
			print_exit("\tError: file could not be opened or does not exist.",
					-1, 2);
		else
		{
			e.argv = ft_strdup(argv[1]);
			if (ft_strcmp(ft_strsub(e.argv, ft_strlen(e.argv) - 4, 4), ".fdf"))
				print_exit("\tError: can only handle .fdf files.", -1, 2);
		}
	}
	else
		print_exit("\tError: at least one parameter is required.", -1, 2);
	if ((e.mlx = mlx_init()))
		init_all(&e);
	else
		print_exit("\tError: MiniLibX could not be initialized.", -1, 2);
	return (0);
}
