/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/23 05:19:08 by mborde            #+#    #+#             */
/*   Updated: 2015/02/09 18:56:12 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	earth(t_pxl *p1, t_pxl *p2)
{
	if (p1->z <= -255)
		return (0x000000);
	else if (p1->z <= 0 || p2->z <= 0)
		return (0x0000FF);
	else if (p1->z <= 50)
		return ((0xFF - p1->z * 3) << 8);
	else if (p1->z <= 120)
		return (((0x80 - p1->z) << 16) + (33 << 8));
	else
		return (0xFFFFFF);
}

static int	forest(t_pxl *p1, t_pxl *p2)
{
	if (p1->z < p2->z || p1->z > p2->z)
		return (0x663300);
	else if (p1->z <= 0)
		return (0x007F2A);
	else
		return (0xF0B001);
}

static int	shadow(t_pxl *p1, t_pxl *p2)
{
	if (p1->z < p2->z || p1->z > p2->z)
		return (0x000000);
	else if (p1->z <= 0)
		return (0x333333);
	else
		return (0xFFFF33);
}

static int	beach(t_pxl *p1, t_pxl *p2)
{
	if (p1->z < p2->z || p1->z > p2->z)
		return (0x3300FF);
	else if (p1->z <= 0)
		return (0xFFFF99);
	else
		return (0x00CCFF);
}

int			get_color(t_pxl *p1, t_pxl *p2, t_env *e)
{
	if (e->color == 0)
		return (earth(p1, p2));
	else if (e->color == 1)
		return (forest(p1, p2));
	else if (e->color == 2)
		return (shadow(p1, p2));
	else if (e->color == 3)
		return (beach(p1, p2));
	else
		return (0xFFFFFF);
}
