/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/11 01:21:34 by mborde            #+#    #+#             */
/*   Updated: 2015/01/21 09:58:38 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

static char		*char_buff(const int fd)
{
	char	*buff;
	char	*t[2];
	int		ret;

	buff = ft_strnew(BUFF_SIZE);
	t[0] = NULL;
	ft_memset(buff, '\0', BUFF_SIZE + 1);
	if ((ret = read(fd, buff, BUFF_SIZE)) >= 0)
	{
		t[1] = t[0];
		t[0] = ft_strjoin(t[1], buff);
		if (t[1])
			free(t[1]);
		if (ft_strchr(t[0], '\n'))
		{
			free(buff);
			return (t[0]);
		}
		ft_memset(buff, '\0', BUFF_SIZE + 1);
	}
	else
		return (NULL);
	free(buff);
	return (t[0]);
}

static t_file	*new_file(int fd)
{
	t_file *file;

	file = (t_file *)ft_memalloc(sizeof(t_file));
	file->fd = fd;
	if (!(file->buff = char_buff(fd)))
		return (NULL);
	file->next = NULL;
	return (file);
}

static t_file	*fd_info(int fd, t_file *file)
{
	t_file	*index;
	t_file	*prev_index;

	index = file;
	if (file == NULL)
	{
		if (!(index = new_file(fd)))
			return (NULL);
	}
	else
	{
		while (index)
		{
			prev_index = index;
			if (index->fd == fd)
				return (index);
			index = index->next;
		}
		if (!(prev_index->next = new_file(fd)))
			return (NULL);
		index = prev_index->next;
	}
	return (index);
}

static int		return_gnl(t_file *file, char **line, int i)
{
	if (!file->buff || (!ft_strchr(file->buff, '\n') && i == 0))
		return (0);
	if (!ft_strchr(file->buff, '\n'))
	{
		*line = file->buff;
		file->buff = NULL;
		return (1);
	}
	if (file->buff[0] == '\n')
	{
		*line = ft_strnew(1);
		file->buff = ft_strsub(file->buff, 1, ft_strlen(file->buff));
	}
	else
	{
		*line = ft_strsub(file->buff, 0, i);
		file->buff = ft_strsub(file->buff, i + 1, ft_strlen(file->buff) - i);
	}
	if (!file->buff)
		file->buff = ft_strnew(1);
	return (1);
}

int				get_next_line(const int fd, char **line)
{
	static t_file	*f[2];
	int				i;

	if (fd < 0 || BUFF_SIZE < 1 || !line)
		return (-1);
	if (!(i = 0) && !f[1])
	{
		if (!(f[1] = fd_info(fd, f[1])))
			return (-1);
		f[0] = f[1];
	}
	else if (!(f[0] = fd_info(fd, f[1])))
		return (-1);
	while (f[0]->buff && f[0]->buff[i] != '\n')
	{
		while (f[0]->buff[i] != '\n' && f[0]->buff[i])
			i++;
		if (f[0]->buff[i] == 0)
		{
			f[0]->buff = ft_strjoin(f[0]->buff, char_buff(f[0]->fd));
			if (!f[0]->buff[i])
				return (return_gnl(f[0], line, i));
		}
	}
	return (return_gnl(f[0], line, i));
}
