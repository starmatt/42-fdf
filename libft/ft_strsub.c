/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 19:42:36 by mborde            #+#    #+#             */
/*   Updated: 2015/01/02 17:23:48 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*cut;
	size_t	i;
	size_t	j;

	i = start;
	j = 0;
	if (s == NULL || s[start] == '\0')
		return (NULL);
	if (!(cut = malloc(sizeof(char) * len + 1)))
		return (NULL);
	while (j < len && s[i])
	{
		cut[j] = (char)s[i];
		i++;
		j++;
	}
	cut[j] = '\0';
	return (ft_strdup(cut));
}
