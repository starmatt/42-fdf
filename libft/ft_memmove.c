/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 17:44:35 by mborde            #+#    #+#             */
/*   Updated: 2014/11/09 17:39:56 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char	*cpy;

	cpy = (char *)malloc(sizeof(char *) * len);
	cpy = ft_strncpy(cpy, src, len);
	dst = (void *)ft_strncpy(dst, cpy, len);
	free(cpy);
	return (dst);
}
