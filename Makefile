# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mborde <mborde@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/12/02 18:06:00 by mborde            #+#    #+#              #
#    Updated: 2015/01/23 08:26:54 by mborde           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf

SRCS = srcs/main.c			\
	   srcs/stock_map.c		\
	   srcs/help.c			\
	   srcs/get_next_line.c	\
	   srcs/fdf.c			\
	   srcs/color.c			\
	   libft/libft.a

INCS = -I incs/

OBJS = $(SRC:.c=.o)

LIBP = -L/usr/X11/lib/ -lmlx -lXext -lX11

FLAG = -Wall -Wextra -Werror

all: $(NAME)

$(NAME):
	@echo "libft >"
	@make -C libft/ fclean
	@make -C libft/
	@echo "fdf >"
	@echo "	Compiling sources..."
	@gcc $(FLAG) -o $(NAME) $(SRCS) $(INCS) $(LIBP)
	@echo "	:D"

clean:
	@/bin/rm -rf $(OBJS)	
	@echo "fdf >"
	@echo "	Object files removed."	

fclean: clean
	@/bin/rm -rf $(NAME)
	@echo "	Executable removed."

cleanlib:
	@make -C libft/ fclean

re: fclean all
